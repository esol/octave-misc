% this script is for making pretty figures with octave
% to use, replace t and X with relevant quantities

%data
%replace with relevant one
t=[-pi:0.01:pi];
X=sin(t);

%plot
plot(t,X,'linewidth',5)

% axis font
set(gca,'linewidth',4,'fontsize',25)

% correct zooming
axis([-3 3 -1.1 1.1])

% axis labels
xlabel('x-axis label','fontsize',25)
ylabel('y-axis label','fontsize',25)

% title
title('Title of plot','fontsize',25)






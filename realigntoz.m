function [R] = realigntoz(vec)
% R = realigntoz([x;y;z])
% R*[x;y;z] == [0;0;r]
% rotation matrix R aligns input vector vec = (x,y,z) to in the direction of z-axis
x=vec(1);
y=vec(2);
z=vec(3);
% rotation angle by the original z-axis
t1=atan(x/y);
% first rotation matrix
R1=[cos(t1), -sin(t1), 0; sin(t1), cos(t1), 0; 0, 0,1];

% new y coordinate
y2 = sin(t1)*x + cos(t1)*y;

% rotation angle by x-axis
t2=atan(y2/z);
% second rotation matrix
R2=[1 0 0; 0 cos(t2) -sin(t2); 0 sin(t2) cos(t2)];

% final rotation matrix
R = R2*R1;

end


